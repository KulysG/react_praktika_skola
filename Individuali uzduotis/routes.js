const UserApiController = require('./controllers/UserApiController')
const userApiController = new UserApiController()
module.exports = (app) => {
    app.get('*', (req, res) => {
        res.render('index')
    })
    app.get('/api/users', userApiController.getUsers)
    app.post('/api/user', userApiController.findUser)
    return app
}
