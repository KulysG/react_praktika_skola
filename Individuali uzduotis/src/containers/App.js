import { connect } from "react-redux"
import AppComponent from "../components/Appcomponent"
import { Add_Question } from '../actions/QuestionAction';


const mapDispatchToProps = dispatch => {
    return {
        onHandleQuestions: (payload) => {
            dispatch(Add_Question(payload))
        }
    }
}
const App = connect(null, mapDispatchToProps)(AppComponent)
export default App