import { connect } from "react-redux"
import AdminComponent from "../components/admin"
import { Solve_Question } from '../actions/QuestionAction';

const mapStateToProps = ({ questionR }) => {
    return {
        questions: questionR.questions,
        isLoaded: questionR.isLoaded
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onHandleSolveQuestion: (payload) => {
            dispatch(Solve_Question(payload))
        }
    }
}

const App = connect(mapStateToProps, mapDispatchToProps)(AdminComponent)
export default App