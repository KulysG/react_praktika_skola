import { ADD_QUESTION, SOLVE_QUESTION } from "../constants";

export function Add_Question(payload) {
    return {
        type: ADD_QUESTION,
        payload
    }
}
export function Solve_Question(payload) {
    return {
        type: SOLVE_QUESTION,
        payload
    }
}