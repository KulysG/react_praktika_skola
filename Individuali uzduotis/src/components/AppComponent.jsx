import React, { useState } from "react"
import { v4 as uuid } from 'uuid';

const problems = ['Kompiuterio Problemos', 'Softo Problemos', 'Tinklo Problemos']
const doorNumber = [152, 300, 450]
function AppComponent({ onHandleQuestions }) {
  const [values, setValues] = useState({problem: "", type:"", cabinet:""});
   
    const handleChange = (e) => {
        e.preventDefault();
        const fieldName =  e.target.id;
        const fieldValue = e.target.value;   
        setValues({ [fieldName]: fieldValue}) ;
 
    }
  
    const onSubmit = () => onHandleQuestions(values);
console.log(values)
    return (
        <div>
            <div className="container py-5" style={{ maxWidth: '720px' }}>
                <h3 className="text-center">Siųsti problėmą</h3>
                <form onSubmit={onSubmit}>
                    <div className="form-group">
                        <label>Problemos aprašymas</label>
                        <input className="form-control" id="problem"value={values.problem} onChange={handleChange}/>
                    </div>
                    <div className="form-group">
                        <label>Problemos tipas</label>
                        <select className="form-control" value={values.type} onChange={handleChange}>
                           {problems.map(problem => <option>{problem}</option>)}
                        </select>
                    </div>
                    <div className="form-group">
                        <label>Kabineto numeris</label>
                        <select className="form-control" value={values.cabinet} onChange={handleChange}>
                        {doorNumber.map(number => <option>{number}</option>)}
                         
                        </select>
                    </div>
                    <div className="form-group">
                        <button onClick={onSubmit} className="btn btn-primary">Siųsti</button>
                    </div>
                </form>
            </div>
        </div>
    )

}
export default AppComponent