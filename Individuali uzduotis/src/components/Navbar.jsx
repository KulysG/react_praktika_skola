import React from 'react';
import { Link, NavLink } from 'react-router-dom';
export default function Navbar() {
    return (
        <div>
            <nav class="navbar navbar-expand-sm navbar-light bg-light">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <Link class="nav-link" to="/" exact >Home</Link>
                            </li>
                            <li class="nav-item">
                                <Link class="nav-link" to="/admin" exact>Admin</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}
