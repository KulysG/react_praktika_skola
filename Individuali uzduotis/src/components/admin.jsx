import React, { useState, useEffect } from 'react'

export default function admin({ questions, onHandleSolveQuestion }) {
    const [Questions, SetQuestions] = useState(questions);
    useEffect(() => {
       const sortquestion= [].concat(questions).sort((a, b) => a.status ? 1 : -1);
       SetQuestions(sortquestion)
    }, [questions])
    console.log
    return (
        <div>
            <div className="container py-5" >
                <h3 className="text-center">Administrator Review</h3>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Problėmos aprašymas</th>
                            <th scope="col">Problema</th>
                            <th scope="col">Kabinetas</th>
                            <th scope="col">Statusas</th>
                            <th scope="col">Pasirinkimas</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            Questions && Questions.map((item, index) => (
                                <tr>
                                    <td>{item.question}</td>
                                    <td>{item.typ}</td>
                                    <td>{item.cbn}</td>
                                    <td>{item.status ? 'Solved' : 'UnSolved'}</td>
                                    <td>
                                        {
                                            !item.status && <button className="btn btn-primary btn-sm" onClick={() => onHandleSolveQuestion({ id: item.id })}>Solve</button>
                                        }
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        </div>
    )
}
