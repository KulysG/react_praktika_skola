import {combineReducers} from "redux"
import questionReducer from './questionReducer';

const allReducers = combineReducers({
    questionR:questionReducer
})

export default allReducers