import {
    ADD_QUESTION,
    SOLVE_QUESTION
} from "../constants"

const initialState = {
    questions: [],
    isLoaded: false
}

function questionReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_QUESTION:
            return {
                questions: [...state.questions, action.payload],
                isLoaded: false
            };
        case SOLVE_QUESTION:
            return {
                questions: state.questions.filter(item => {
                    if (item.id === action.payload.id) {
                        return item.status = true;
                    }
                    return item;
                }),
                isLoaded: false
            }
        default:
            return state
    }

}
export default questionReducer