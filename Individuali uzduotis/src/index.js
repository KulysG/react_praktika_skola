import React from "react"
import { render } from "react-dom"
import { Provider } from "react-redux"
import App from "./containers/App"
import mainStore from "./store"
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navbar from "./components/Navbar";
import AdminPreview from "./containers/Admin"

const store = mainStore()

render(
  <Provider store={store}>
    <Router>
      <Navbar />
      <Switch>
        <Route path="/" exact >
          <App />
        </Route>
        <Route path="/admin" exact >
          <AdminPreview />
        </Route>
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("app")
)