import {
    SUCCESS_USERS_RESULT,
    FAILURE_RESULT,
    REQUEST_USERS
}from "../constants"

const initialState ={
    users: [],
    isFetching: false,
    isLoaded: false
}

function userReducer(state = initialState, action){
    switch (action.type) {
        case SUCCESS_USERS_RESULT:
            return {
                ...state,
                isFetching: false,
                users: action.users,
                isLoaded: true
            };
            case REQUEST_USERS:
                return {
                    ...state,
                    isFetching: true
                };
            case FAILURE_RESULT:
                return {
                    isFetching: false,
                    error: action.error
                };
                default:
                    return state
    }
 
}
export default userReducer