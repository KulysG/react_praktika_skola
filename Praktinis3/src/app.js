import React, {Component} from 'react'

class App extends Component {

constructor(props){
    super(props)
    this.onHandleUsers = this.onHandleUsers.bind(this)
    this.state ={
        isLoaded: false,
        users: [],

    }
    
}
onHandleUsers(){
    fetch("http://localhost:3000/api/users")
    .then(res => res.json())
    .then(result =>{
        this.setState({
            isLoaded:true,
            users: result,
        })
    })
    .catch(e => console.log(e))
}
onHandleCurrentUser(userId){
    const data = {userId: userId}
    fetch("http://localhost:3000/api/user",{
        method:'POST',
        headers: {"Content-Type":"application/json"},
        body: JSON.stringify(data)
    })
    .then(res=> res.json())
    .then(result =>{
        this.setState({
            isLoaded:false,
            currentUser:result
        })
    })
    .catch(e=>console.log(e))
}
render(){
    const {currentUser} = this.state
    return(
		<div>
       
		<ul>
		{
		this.state.isLoaded && this.state.users.map(item => {
		return(
		<li key={item.id}> Vardas: {item.name}<div>
		Pavarde: {item.surname}</div></li>
		)
		})
		}
		</ul>
        <button onClick={ e=> this.onHandleCurrentUser(1)}>Gauti pirma vartotoja</button>
        <button onClick={ e=> this.onHandleCurrentUser(2)}>Gauti antra vartotoja</button>
        <div>
            {
                currentUser && <div>{currentUser.name} {currentUser.surname}</div>
            }
        </div>
        </div>
    )
           
   }
}

export default App