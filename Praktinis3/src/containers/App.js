import { connect } from "react-redux"
import AppComponent from "../components/Appcomponent"
import { fetchUsers} from "../actions/userActions"

const mapStateToProps = ({ usersR}) => {
    return {
        users: usersR.users,
        isLoaded:usersR.isLoaded
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onHandleUsers: () =>
        {
            dispatch(fetchUsers())
        }
    }
}

const App = connect(mapStateToProps, mapDispatchToProps)(AppComponent)
export default App