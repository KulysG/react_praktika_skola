import {SUCCESS_USERS_RESULT, REQUEST_USERS, FAILURE_RESULT} from "../constants"


export function fetchUsers(){
    return dispatch => {
        dispatch(responseUsers());
    };
}

function requestUsers(){
    return {
        type: REQUEST_USERS,
    }
}

function successUsersResult(users) {
    return {
        type: SUCCESS_USERS_RESULT,
        users
    }
}

function failureResult(error) {
    return{
        type: FAILURE_RESULT,
        error
    }
}

function responseUsers() {
    return dispatch => {
        dispatch(requestUsers())
        return fetch("http://localhost:3000/api/users", {
            headers:{
                "Content-Type":"application/json"
        },
        method: "GET"
        })
        .then(res => {
            return res.json()
        })
        .then(users => {
            dispatch(successUsersResult(users));
        })
        .catch(error => {
            dispatch(failureResult(error));
        })
    }
}
