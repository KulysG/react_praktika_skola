import React from "react"

function AppComponent(props) {
    return (
        <div>
        <button onClick={props.onHandleUsers}>Gauti vartotojus</button>
        <ul>
         {props.isLoaded &&
        props.users.map(item => {
            return (
                <li key={item.id}> vardas:{item.name}, pavarde: {item.surname}
                </li>
            )
        })}
        </ul>
        </div>
    )
    
}
export default AppComponent