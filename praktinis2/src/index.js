import React from 'react';
import ReactDOM from 'react-dom';
import App from './app'
const title = 'Pavyzdinė React-aplikacija';
ReactDOM.render(
    <App/>,
  document.getElementById('app')
)
