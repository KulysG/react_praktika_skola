class UserApiController {
    constructor(){
        this.getUsers = this.getUsers.bind(this)
        this.findUser = this.findUser.bind(this)
    }
  
    _usersList(){
        return [
            {id: 1, name: 'Jonas', surname: 'Grybas'},
            {id: 2, name: 'Gediminas', surname: 'Kulys'}
        ]
    }

    getUsers(req, res, next) {
        const users = this._usersList()
        return res.json(users)
    }
    findUser(req,res,next){
        const userId=parseInt(req.body.userId)||0
        const users = this._usersList()
        const amount = users.length
        let result ={}
        for (let i = 0; i < amount; i++){
            let user = users [i]
            if (user.id == userId){
                result = user
                break
            }
        }
        return res.json(result)
    }
}


module.exports = UserApiController